﻿using OdinSerializer.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;

static class RaceManager
{
    static Dictionary<string, Race> RaceDictionary;
    static internal List<string> PickableRaces;

    static RaceManager()
    {
        RaceDictionary = new Dictionary<string, Race>();
        PickableRaces = new List<string>();
        CultureInfo baseCult = CultureInfo.CurrentCulture;
        CultureInfo newCult = CultureInfo.GetCultureInfo("en-US");
        Thread.CurrentThread.CurrentCulture = newCult;

        var files = Directory
            .GetFiles(Path.Combine(Application.streamingAssetsPath, "Races"), "*.txt")
            .ToList();
        files.AddRange(Directory.GetFiles(State.RaceDirectory, "*.txt"));
        foreach (string fileLoc in files)
        {
            try
            {
                var lines = File.ReadAllLines(fileLoc);
                string raceName = Path.GetFileNameWithoutExtension(fileLoc);
                Race race = new Race();
                race.Name = raceName;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].Contains("Parent Race:"))
                    {
                        race.ParentRaceString = lines[i].Split(':')[1].Trim();
                    }
                    else if (lines[i].Contains("Selectable:"))
                    {
                        var result = lines[i].Split(':')[1].Trim();
                        if (result.ToLower() == "true")
                            race.PickableRace = true;
                        else
                            race.PickableRace = false;
                    }
                    else if (lines[i].Contains("Named Character:"))
                    {
                        var result = lines[i].Split(':')[1].Trim();
                        if (result.ToLower() == "true")
                            race.NamedCharacter = true;
                        else
                            race.NamedCharacter = false;
                    }
                    else if (lines[i].Contains("Hair Color"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.HairColorFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.HairColorMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.HairColorFemale = strings.Skip(1).ToArray();
                            race.HairColorMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Eye Color"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.EyeColorFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.EyeColorMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.EyeColorFemale = strings.Skip(1).ToArray();
                            race.EyeColorMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Hip Description"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.HipDescriptionFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.HipDescriptionMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.HipDescriptionFemale = strings.Skip(1).ToArray();
                            race.HipDescriptionMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Shoulder Description"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.ShoulderDescriptionFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.ShoulderDescriptionMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.ShoulderDescriptionFemale = strings.Skip(1).ToArray();
                            race.ShoulderDescriptionMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Hair Length"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.HairLengthFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.HairLengthMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.HairLengthFemale = strings.Skip(1).ToArray();
                            race.HairLengthMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Hair Style"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.HairStyleFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.HairStyleMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.HairStyleFemale = strings.Skip(1).ToArray();
                            race.HairStyleMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Height"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.HeightRangeFemale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.HeightRangeMale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else
                        {
                            race.HeightRangeFemale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                            race.HeightRangeMale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                        }
                    }
                    else if (lines[i].Contains("Weight"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.WeightRangeFemale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.WeightRangeMale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else
                        {
                            race.WeightRangeFemale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                            race.WeightRangeMale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                        }
                    }
                    else if (lines[i].Contains("Breast Size"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.BreastSizeFemale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.BreastSizeMale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else
                        {
                            race.BreastSizeFemale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                            race.BreastSizeMale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                        }
                    }
                    else if (lines[i].Contains("Dick Size"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.DickSizeFemale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.DickSizeMale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else
                        {
                            race.DickSizeFemale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                            race.DickSizeMale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                        }
                    }
                    else if (lines[i].Contains("Ball Size"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.BallSizeFemale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.BallSizeMale = new float[2]
                            {
                                Convert(strings[2]),
                                Convert(strings[3])
                            };
                        }
                        else
                        {
                            race.BallSizeFemale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                            race.BallSizeMale = new float[2]
                            {
                                Convert(strings[1]),
                                Convert(strings[2])
                            };
                        }
                    }
                    else if (lines[i].Contains("Quirks"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.QuirksFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.QuirksMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.QuirksFemale = strings.Skip(1).ToArray();
                            race.QuirksMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Traits"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 2)
                            continue;
                        strings = Trim(strings);
                        if (strings[1] == "Feminine")
                        {
                            race.TraitsFemale = strings.Skip(2).ToArray();
                        }
                        else if (strings[1] == "Masculine")
                        {
                            race.TraitsMale = strings.Skip(2).ToArray();
                        }
                        else
                        {
                            race.TraitsFemale = strings.Skip(1).ToArray();
                            race.TraitsMale = strings.Skip(1).ToArray();
                        }
                    }
                    else if (lines[i].Contains("Custom"))
                    {
                        var strings = lines[i].Split(',');
                        if (strings.Length < 3)
                            continue;
                        strings = Trim(strings);
                        if (strings[2] == "Feminine")
                        {
                            race.FeminineTag[strings[1]] = strings.Skip(3).ToArray();
                        }
                        else if (strings[2] == "Masculine")
                        {
                            race.MasculineTag[strings[1]] = strings.Skip(3).ToArray();
                        }
                        else
                        {
                            race.MasculineTag[strings[1]] = strings.Skip(2).ToArray();
                            race.FeminineTag[strings[1]] = strings.Skip(2).ToArray();
                        }
                    }
                }
                RaceDictionary[raceName.ToLower()] = race;
                if (race.PickableRace)
                    PickableRaces.Add(raceName);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                State.InitializeErrors.Add(
                    $"Race {Path.GetFileName(fileLoc)} ran into an error while reading, skipping that race."
                );
            }
        }
        PickableRaces = PickableRaces.OrderBy((s) => s).ToList();
        Thread.CurrentThread.CurrentCulture = baseCult;

        foreach (var kvp in RaceDictionary)
        {
            if (
                kvp.Value.ParentRaceString != null
                && RaceDictionary.TryGetValue(kvp.Value.ParentRaceString.ToLower(), out Race race)
            )
            {
                kvp.Value.ParentRace = race;
            }
            else
            {
                if (kvp.Value.ParentRaceString != "none")
                    UnityEngine.Debug.LogWarning(
                        $"Couldn't find parent race {kvp.Value.ParentRaceString} for race {kvp.Key}"
                    );
            }
        }

        float Convert(string value)
        {
            if (float.TryParse(value, out float result))
                return result;
            else
                return 0;
        }

        string[] Trim(string[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = values[i].Trim();
            }
            return values;
        }
    }

    static internal Race GetRace(string race)
    {
        if (
            race.IsNullOrWhitespace() == false
            && RaceDictionary.TryGetValue(race.ToLower(), out Race Race)
        )
        {
            return Race;
        }
        if (RaceDictionary.ContainsKey("human"))
            return RaceDictionary["human"];
        if (State.GameManager != null)
            State.GameManager.CreateMessageBox(
                "Couldn't find the human race, the program is really not equipped for this, strange behavior may result.  If you deleted the file, you should restore it and restart the game.  (If you want humans to simply not be selectable as an option, just set the selectable to false in human.txt)"
            );
        else
            State.InitializeErrors.Add(
                "Couldn't find the human race, the program is really not equipped for this, strange behavior may result.  If you deleted the file, you should restore it and restart the game.  (If you want humans to simply not be selectable as an option, just set the selectable to false in human.txt)"
            );
        return new Race();
    }

    /// <summary>
    /// Gets how many steps down the tree a race is, i.e. Demi-Fox as the child and Demi-Human as the parent would be 1.  Is -1 if no connection.
    /// </summary>
    /// <param name="childRace"></param>
    /// <param name="parentRace"></param>
    /// <returns></returns>
    static internal int RaceDescendants(string childRace, string parentRace)
    {
        if (childRace == "Any")
            return 0;
        if (parentRace == "Any")
            parentRace = "Human";
        if (childRace == parentRace)
            return 0;
        if (childRace == null || parentRace == null)
            return -1;
        if (RaceDictionary.TryGetValue(childRace.ToLower(), out Race race))
        {
            for (int i = 1; i < 10; i++)
            {
                if (race.ParentRaceString == parentRace)
                    return i;
                if (race.ParentRace != null)
                    race = race.ParentRace;
                else
                    break;
            }
        }
        return -1;
    }
}
