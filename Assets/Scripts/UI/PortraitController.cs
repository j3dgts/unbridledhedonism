using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

enum PortraitFlag
{
    Dead,
    Alive, // New, implied when dead isn't set.
    Eaten,
    Full,
    Swallowing,
    BeingSwallowed,
    Both, // Implies Belly/Balls.
    Belly, // New.
    Balls,
    Stomach,
    Bowels,
    Womb,
    Unwilling,
    Weakened,
    Healthy,
    Nude,
    Underwear,
    Undressed,
    Disposal,
    NonDisposal, // Implied when Disposal is not set.
    Horny,
    Absorbing,
    Multiple,
    Hungry,
    CV,
    UB,
    AV,
    OV,
    Digesting, // If this person is eaten and being digested.
    Safe, // If this person is eaten and not being digested.

    // weight gain related
    Gain, // Implies Gain +10% or more
    Weight, // weight gain
    Breast, // breast size
    Dick, // dick/balls size

    // size diff related
    Shrink, // affected by shrink spell
    Tiny, // affected by shrink and < 2ft tall
    Grow, // affected by grow spell
    Huge, // affected by grow and > 12ft tall

    Fem, // use with "default", for chars w/ boobs
    Masc, // use with "default", for chars w/o boobs
}

class PortraitController
{
    HashSet<string> PicturesUnloaded;
    HashSet<string> PicturesAny;
    Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>> Pictures;

    public PortraitController()
    {
        PicturesUnloaded = new HashSet<string>();
        Pictures = new Dictionary<string, Dictionary<HashSet<PortraitFlag>, Sprite>>();

        foreach (string file in GetPictureFiles())
        {
            if (file.ToLower().Contains("-"))
            {
                var baseName = Path.GetFileNameWithoutExtension(file).Split('-')[0];
                PicturesUnloaded.Add(baseName.ToLower());
            }
            else
            {
                PicturesUnloaded.Add(Path.GetFileNameWithoutExtension(file).ToLower());
            }
        }

        PicturesAny = new HashSet<string>(PicturesUnloaded);

        if (PicturesUnloaded.Contains("default") && Config.DisableDefaultImages == false)
        {
            AttemptLoad("Default");
            PicturesUnloaded.Remove("default");
        }
    }

    /// <summary>Return all valid picture files.</summary>
    internal static List<string> GetPictureFiles()
    {
        var files = Directory
            .GetFiles(
                Path.Combine(Application.streamingAssetsPath, "Pictures"),
                "*.*",
                SearchOption.AllDirectories
            )
            .ToList();
        files.AddRange(Directory.GetFiles(State.PicDirectory, "*.*", SearchOption.AllDirectories));
        return files
            .Where(
                s =>
                    s.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase)
                    || s.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase)
            )
            .ToList();
    }

    /// Return the plain hash set of flags from a contiguous string of flag names without more processing.
    internal static HashSet<PortraitFlag> ParseStringToFlagsRaw(string str)
    {
        if (String.IsNullOrWhiteSpace(str))
            return new HashSet<PortraitFlag>();
        str = str.ToLower();
        foreach (
            var flagName in Enum.GetNames(typeof(PortraitFlag)).OrderByDescending(x => x.Length)
        )
        {
            if (!str.StartsWith(flagName.ToLower()))
                continue;
            var flags = ParseStringToFlags(str.Substring(flagName.Length));
            flags.Add((PortraitFlag)Enum.Parse(typeof(PortraitFlag), flagName));
            return flags;
        }

        if (UnityEngine.Object.FindObjectOfType<MessageBox>() == null)
            State.GameManager.CreateMessageBox(
                $"Did not understand condition \"{str}\" in image filename"
            );

        throw new Exception($"Can not parse next flag from {str} in image filename");
    }

    /// Return a hash set of flags from a contiguous string of flag names.
    internal static HashSet<PortraitFlag> ParseStringToFlags(string str)
    {
        var flags = ParseStringToFlagsRaw(str);
        if (flags.Count == 0)
            return flags; // Default image.
        // Keep both/belly/balls flags consistent.
        if (flags.Contains(PortraitFlag.Both))
        {
            flags.Add(PortraitFlag.Belly);
            flags.Add(PortraitFlag.Balls);
        }
        if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls))
            flags.Add(PortraitFlag.Both);
        return flags;
    }

    internal void AttemptLoad(string name)
    {
        foreach (string file in GetPictureFiles())
        {
            var splits = Path.GetFileNameWithoutExtension(file).Split('-');

            if (splits[0].ToLower() != name.ToLower())
                continue;

            name = name.ToLower();
            if (!Pictures.ContainsKey(name))
            {
                Pictures.Add(name, new Dictionary<HashSet<PortraitFlag>, Sprite>());
            }
            Pictures[name].Add(ParseStringToFlags(splits.ElementAtOrDefault(1)), LoadPNG(file));
        }
    }

    /// Return the 'best' picture for `name` with `flags`.
    /// The best picture is the one with the most conditions where all conditions are fulfilled.
    Sprite GetBestMatch(string name, Person person, HashSet<PortraitFlag> flags)
    {
        name = name.ToLower();
        if (!Pictures.ContainsKey(name))
            name = "default";
        if (!Pictures.ContainsKey(name))
            return null;

        var bestSets = Pictures[name].Keys
            .Where(s => s.IsSubsetOf(flags))
            .OrderByDescending(s => s.Count);

        // if gone, no photo
        if (person.Health <= Constants.EndHealth)
            return null;

        // if dead, display a dead photo if it exists
        if (person.Health <= 0)
        {
            bestSets = Pictures[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Dead))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                if (Pictures[name].ContainsKey(best))
                    return Pictures[name][best];
            }
            ;
        }

        // if eaten, display an eaten photo if it exists
        if (person.BeingEaten)
        {
            // if eaten and full, show that over regular eaten photos
            if (person.VoreController.HasPrey(VoreLocation.Any))
            {
                bestSets = Pictures[name].Keys
                    .Where(
                        s =>
                            s.IsSubsetOf(flags)
                            && flags.Contains(PortraitFlag.Eaten)
                            && flags.Contains(PortraitFlag.Full)
                    )
                    .OrderByDescending(s => s.Count);
                foreach (var best in bestSets)
                {
                    if (Pictures[name].ContainsKey(best))
                        return Pictures[name][best];
                }
                ;
            }
            else
            {
                bestSets = Pictures[name].Keys
                    .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Eaten))
                    .OrderByDescending(s => s.Count);
                foreach (var best in bestSets)
                {
                    if (Pictures[name].ContainsKey(best))
                        return Pictures[name][best];
                }
                ;
            }
        }

        // if disposing, display a disposal photo if it exists
        if (
            person.StreamingSelfAction == SelfActionType.CockDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.CockDisposalFloor
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor
            || person.StreamingSelfAction == SelfActionType.ScatDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.ScatDisposalFloor
        )
        {
            bestSets = Pictures[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Disposal))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                if (Pictures[name].ContainsKey(best))
                    return Pictures[name][best];
            }
            ;
        }

        // if full, display a full photo if it exists
        if (person.VoreController.HasPrey(VoreLocation.Any))
        {
            bestSets = Pictures[name].Keys
                .Where(s => s.IsSubsetOf(flags) && flags.Contains(PortraitFlag.Full))
                .OrderByDescending(s => s.Count);
            foreach (var best in bestSets)
            {
                if (Pictures[name].ContainsKey(best))
                    return Pictures[name][best];
            }
            ;
        }

        // finally, check any other case
        bestSets = Pictures[name].Keys
            .Where(s => s.IsSubsetOf(flags))
            .OrderByDescending(s => s.Count);
        foreach (var best in bestSets)
        {
            if (Pictures[name].ContainsKey(best))
                return Pictures[name][best];
        }
        ;

        Debug.LogError("No picture found.  Missing a picture with no conditions.");
        return null;
    }

    internal Sprite GetPicture(Person person)
    {
        string str = person.Picture;
        if (string.IsNullOrWhiteSpace(str))
            return null;
        if (
            PicturesAny.Contains(str.ToLower()) == false && PicturesAny.Contains("default") == false
        )
            return null;
        if (PicturesUnloaded.Contains(str.ToLower()))
        {
            AttemptLoad(person.Picture);
            PicturesUnloaded.Remove(str.ToLower());
        }
        var flags = new HashSet<PortraitFlag>();

        // DEFAULT PORTRAIT GENDER CHECKS
        if (PicturesAny.Contains(str.ToLower()) == false && PicturesAny.Contains("default"))
        {
            if (person.GenderType.HasBreasts)
                flags.Add(PortraitFlag.Fem);
            else
                flags.Add(PortraitFlag.Masc);
        }

        // DEAD CHECKS
        if (person.Health <= 0)
        {
            flags.Add(PortraitFlag.Dead);
            var vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();
            if (vorelocation == "balls")
                flags.Add(PortraitFlag.CV);
            if (vorelocation == "womb")
                flags.Add(PortraitFlag.UB);
            if (vorelocation == "stomach")
                flags.Add(PortraitFlag.OV);
            if (vorelocation == "bowels")
                flags.Add(PortraitFlag.AV);
        }
        else
        {
            flags.Add(PortraitFlag.Alive);
        }

        // PREY CHECKS
        if (person.BeingEaten && person.Health > Constants.EndHealth)
        {
            if (Config.AltBellyImage)
            {
                var parent = GetPicture(person.FindMyPredator());
                if (parent != null)
                    return parent;
            }
            
            flags.Add(PortraitFlag.Eaten);

            if (person.FindMyPredator().VoreController.GetProgressOf(person).IsSwallowing())
                flags.Add(PortraitFlag.BeingSwallowed);

            if (person.FindMyPredator().VoreController.GetProgressOf(person).Willing == false)
                flags.Add(PortraitFlag.Unwilling);

            if (person.FindMyPredator().VoreController.TargetIsBeingDigested(person))
                flags.Add(PortraitFlag.Digesting);
            else
                flags.Add(PortraitFlag.Safe);

            var vorelocation = person.VoreTracking.LastOrDefault().Location.ToString().ToLower();
            if (vorelocation == "balls")
                flags.Add(PortraitFlag.CV);
            if (vorelocation == "womb")
                flags.Add(PortraitFlag.UB);
            if (vorelocation == "stomach")
                flags.Add(PortraitFlag.OV);
            if (vorelocation == "bowels")
                flags.Add(PortraitFlag.AV);
        }

        // DISPOSAL CHECKS
        if (
            person.StreamingSelfAction == SelfActionType.CockDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.CockDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.CV);
        }
        if (
            person.StreamingSelfAction == SelfActionType.UnbirthDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.UnbirthDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
            flags.Add(PortraitFlag.UB);
        }
        if (
            person.StreamingSelfAction == SelfActionType.ScatDisposalBathroom
            || person.StreamingSelfAction == SelfActionType.ScatDisposalFloor
        )
        {
            flags.Add(PortraitFlag.Disposal);
        }
        if (!flags.Contains(PortraitFlag.Disposal))
            flags.Add(PortraitFlag.NonDisposal);

        // PREDATOR CHECKS

        if (person.VoreController.HasPrey(VoreLocation.Any))
        {
            flags.Add(PortraitFlag.Full);

            if (person.VoreController.Swallowing(VoreLocation.Any))
                flags.Add(PortraitFlag.Swallowing);

            if (person.VoreController.HasBellyPrey())
            {
                flags.Add(PortraitFlag.Belly);
                if (person.VoreController.BellySize() > 1.5)
                    flags.Add(PortraitFlag.Multiple);
            }

            if (person.VoreController.HasPrey(VoreLocation.Balls))
            {
                flags.Add(PortraitFlag.Balls);
                if (person.VoreController.BallsSize() > 1.5)
                    flags.Add(PortraitFlag.Multiple);
            }

            if (flags.Contains(PortraitFlag.Belly) && flags.Contains(PortraitFlag.Balls))
                flags.Add(PortraitFlag.Both);
            if (person.VoreController.HasPrey(VoreLocation.Stomach))
                flags.Add(PortraitFlag.Stomach);
            if (person.VoreController.HasPrey(VoreLocation.Bowels))
                flags.Add(PortraitFlag.Bowels);
            if (person.VoreController.HasPrey(VoreLocation.Womb))
                flags.Add(PortraitFlag.Womb);

            if (
                person.VoreController.HasPrey(VoreLocation.Any)
                && person.VoreController.BellySize() < 0.7
                && person.VoreController.BallsSize() < 0.7
            )
            {
                flags.Add(PortraitFlag.Absorbing);
            }
        }

        // WEIGHT GAIN CHECKS
        var wgThreshold = 1.1f; // at +10% growth, display WG images
        if (
            person.MiscStats.CurrentWeightGain >= wgThreshold
            || person.MiscStats.CurrentBreastGain >= wgThreshold
            || person.MiscStats.CurrentDickGain >= wgThreshold
        )
        {
            flags.Add(PortraitFlag.Gain);
            if (person.MiscStats.CurrentWeightGain >= wgThreshold)
                flags.Add(PortraitFlag.Weight);
            if (person.MiscStats.CurrentBreastGain >= wgThreshold)
                flags.Add(PortraitFlag.Breast);
            if (person.MiscStats.CurrentDickGain >= wgThreshold)
                flags.Add(PortraitFlag.Dick);
        }

        // SIZE DIFF CHECKS
        if (person.Magic.Resize_Level > 0)
        {
            flags.Add(PortraitFlag.Grow);
            if (person.PartList.Height > 144) // if greater than 12 ft tall and grown, get "Huge"
                flags.Add(PortraitFlag.Huge);
        }
        if (person.Magic.Resize_Level < 0)
        {
            flags.Add(PortraitFlag.Shrink);
            if (person.PartList.Height < 24) // if less than 2 ft tall and shrunk, get "Tiny"
                flags.Add(PortraitFlag.Tiny);
        }

        // GENERAL CHECKS (NOT VORE RELATED)
        if (person.Health < Constants.HealthMax / 1.5)
            flags.Add(PortraitFlag.Weakened);
        if (person.Health == Constants.HealthMax)
            flags.Add(PortraitFlag.Healthy);
        if (
            person.Needs.Horniness > .8f
            || person.StreamingSelfAction == SelfActionType.Masturbate
            || person.ActiveSex != null
        )
            flags.Add(PortraitFlag.Horny);
        if (person.Needs.Hunger > .6f || person.Magic.Duration_Hunger > 0)
            flags.Add(PortraitFlag.Hungry);
        switch (person.ClothingStatus)
        {
            case ClothingStatus.Nude:
                flags.Add(PortraitFlag.Nude);
                flags.Add(PortraitFlag.Undressed);
                break;
            case ClothingStatus.Underwear:
                flags.Add(PortraitFlag.Underwear);
                flags.Add(PortraitFlag.Undressed);
                break;
            case ClothingStatus.Normal:
                break;
            default:
                break;
        }
        return GetBestMatch(str, person, flags);
    }

    static Sprite LoadPNG(string filePath)
    {
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2, TextureFormat.BGRA32, false);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        if (tex == null)
            return null;

        Rect rect = new Rect(new Vector2(0, 0), new Vector2(tex.width, tex.height));
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        int higherDimension = Math.Max(tex.width, tex.height);

        if (Config.CropPortraits)
        {
            var texRatio = ((float)tex.height / (float)tex.width);
            var targetRatio = 1.6;

            if (texRatio > targetRatio) // adjust an image that is too tall
            {
                var heightAdj = (int)(tex.width * targetRatio);
                rect.Set(0, (tex.height - heightAdj), tex.width, heightAdj);
                higherDimension = Math.Max(tex.width, heightAdj);
            }
        }
                
        Sprite sprite = Sprite.Create(tex, rect, pivot, higherDimension);
        return sprite;
    }
}
