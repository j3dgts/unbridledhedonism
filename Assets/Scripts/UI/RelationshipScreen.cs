﻿using System.Text;
using UnityEngine;
using TMPro;

public class RelationshipScreen : MonoBehaviour
{
    public TMP_Dropdown CharacterDropdown;
    public TextMeshProUGUI InfoText;

    public void Open()
    {
        gameObject.SetActive(true);
        CharacterDropdown.ClearOptions();
        int playerIndex = 0;
        int count = 0;
        foreach (Person person in State.World.GetPeople(true))
        {
            CharacterDropdown.options.Add(
                new TMP_Dropdown.OptionData($"{person.FirstName} {person.LastName}")
            );
            if (person == State.World.ControlledPerson)
                playerIndex = count;
            count++;
        }
        CharacterDropdown.value = playerIndex;
        CharacterDropdown.RefreshShownValue();
        UpdateRelations();
    }

    public void UpdateRelations()
    {
        StringBuilder sb = new StringBuilder();
        foreach (Person source in State.World.GetPeople(true))
        {
            if (CharacterDropdown.captionText.text != $"{source.FirstName} {source.LastName}")
                continue;
            foreach (Person target in State.World.GetPeople(true))
            {
                if (source == target)
                    continue;

                sb.AppendLine($"<b>{target.FirstName} {target.LastName}</b>");

                if (source.HasRelationshipWith(target) == false)
                {
                    sb.AppendLine($"Haven't met them yet");
                    sb.AppendLine($" ");
                    continue;
                }

                HelperFunctions.GetRelationships(ref sb, source, target);

                if (target.Romance.Dating == source)
                    sb.AppendLine($"<i>{target.FirstName} and {source.FirstName} are dating!</i>");
                if (target.GetRelationshipWith(source).HaveHadSex)
                    sb.AppendLine(
                        $"<i>{target.FirstName} and {source.FirstName} have had sex!</i>"
                    );

                if (source.GetRelationshipWith(target).HasEaten)
                    if (source.GetRelationshipWith(target).HasDigested)
                        sb.AppendLine(
                            $"<i>{source.FirstName} has digested {target.FirstName}!</i>"
                        );
                    else
                        sb.AppendLine(
                            $"<i>{source.FirstName} has swallowed {target.FirstName}!</i>"
                        );

                if (source.GetRelationshipWith(target).HasBeenEatenBy)
                    if (source.GetRelationshipWith(target).HasBeenDigestedBy)
                        sb.AppendLine(
                            $"<i>{target.FirstName} has digested {source.FirstName}!</i>"
                        );
                    else
                        sb.AppendLine(
                            $"<i>{target.FirstName} has swallowed {source.FirstName}!</i>"
                        );

                if (
                    target.FindBestFriend() == source
                    && target.GetRelationshipWith(source).FriendshipLevel >= 0.5f
                )
                    sb.AppendLine(
                        $"<i>{source.FirstName} is {target.FirstName}'s best friend!</i>"
                    );
                if (
                    target.FindCrush() == source
                    && target.GetRelationshipWith(source).RomanticLevel >= 0.3f
                )
                    sb.AppendLine($"<i>{source.FirstName} is {target.FirstName}'s #1 crush!</i>");
                if (
                    target.FindNemesis() == source
                    && target.GetRelationshipWith(source).FriendshipLevel <= -0.3f
                )
                    sb.AppendLine(
                        $"<i>{source.FirstName} is {target.FirstName}'s worst enemy!</i>"
                    );
                if (target.FindVendetta() == source)
                    sb.AppendLine(
                        $"<i>{target.FirstName} has a vendetta against {source.FirstName}!</i>"
                    );
                sb.AppendLine($" ");
            }
            break;
        }
        InfoText.text = sb.ToString();
    }
}
