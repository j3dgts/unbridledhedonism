﻿using OdinSerializer;
using static HelperFunctions;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Magic
{
    [OdinSerialize]
    private Person Self;

    [OdinSerialize]
    private float _maxmana;

    [OdinSerialize]
    private float _mana;

    [OdinSerialize]
    private float _manaregen;

    [OdinSerialize]
    private float _potency;

    [OdinSerialize]
    private float _proficiency;

    [OdinSerialize]
    private float _willpower;

    [OdinSerialize]
    private int _duration_passdoor;

    [OdinSerialize]
    private int _resized_level;

    [OdinSerialize]
    private int _resized_duration;

    [OdinSerialize]
    private int _duration_freeze;

    [OdinSerialize]
    private int _duration_hunger;

    [OdinSerialize]
    private int _duration_charm;

    [OdinSerialize]
    private int _duration_aroused;

    [OdinSerialize]
    private int _duration_preycurse;

    public Magic(Person person)
    {
        _maxmana = 3;
        _mana = _maxmana;
        _manaregen = 1;
        _potency = Rand.NextFloat(0, 1f);
        _proficiency = Rand.NextFloat(0, 0.3f);
        _willpower = Rand.NextFloat(0, 0.3f);
        _duration_passdoor = 0;
        _duration_charm = 0;
        _duration_aroused = 0;
        _duration_hunger = 0;
        _duration_preycurse = 0;
        Self = person;
    }

    [OdinSerialize]
    [ProperName("Can Cast Spells")]
    [Description(
        "Allows the character to cast spells and use magic. Can be overridden by world settings."
    )]
    [Category("Magic")]
    public bool CanCast = false;

    [Description(
        "The maximum mana this character can contain at once, not including trait bonuses."
    )]
    [Category("Magic")]
    [FloatRange(0, 10)]
    public float MaxMana
    {
        get => _maxmana;
        set
        {
            _maxmana = value;
            if (_maxmana < 0)
                _maxmana = 0;
        }
    }

    [VariableEditorIgnores]
    [Description("The current mana this character has available for spellcasting.")]
    [Category("Magic")]
    [FloatRange(0, 99)]
    public float Mana
    {
        get => _mana;
        set
        {
            _mana = value;
            if (_mana < 0)
                _mana = 0;
            if (_mana > (Self.Magic.MaxMana + Self.Boosts.MaxMana))
                _mana = Self.Magic.MaxMana + Self.Boosts.MaxMana;
        }
    }

    [Description("How quickly the character generates mana.")]
    [Category("Magic")]
    [FloatRange(0, 5)]
    public float ManaRegen
    {
        get => _manaregen;
        set
        {
            _manaregen = value;
            if (_manaregen < 0)
                _manaregen = 0;
        }
    }

    [Description(
        "The potency of a character's spells. Affects duration and magnitude of healing/charm effects."
    )]
    [Category("Magic")]
    [FloatRange(0, 1)]
    public float Potency
    {
        get => _potency;
        set
        {
            _potency = value;
            if (_potency < 0)
                _potency = 0;
            if (_potency > 1)
                _potency = 1;
        }
    }

    [Description(
        "How adept a character is at casting spells. Boosted by studying, decays over time."
    )]
    [Category("Magic")]
    [FloatRange(0, 1)]
    public float Proficiency
    {
        get => _proficiency;
        set
        {
            _proficiency = value;
            if (_proficiency < 0)
                _proficiency = 0;
            if (_proficiency > 1)
                _proficiency = 1;
        }
    }

    [Description(
        "How adept a character is at resisting spells. Boosted by meditating, decays over time."
    )]
    [Category("Magic")]
    [FloatRange(0, 1)]
    public float Willpower
    {
        get => _willpower;
        set
        {
            _willpower = value;
            if (_willpower < 0)
                _willpower = 0;
            if (_willpower > 1)
                _willpower = 1;
        }
    }

    // HACKY METHOD TO MAKE TEXTS WORK

    [VariableEditorIgnores]
    [OdinSerialize]
    [ProperName("Self Spell Succeeded")]
    [Description("A hacky way to make texts work for self-casts which can fail sometimes.")]
    public bool SelfCastSuccess = false;

    // DURATIONS

    [Description("How long the passwall effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Passdoor
    {
        get => _duration_passdoor;
        set
        {
            _duration_passdoor = value;
            if (_duration_passdoor < 0)
                _duration_passdoor = 0;
        }
    }

    [Description("How long the resize effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Resized
    {
        get => _resized_duration;
        set
        {
            _resized_duration = value;
            if (_resized_duration < 0)
                _resized_duration = 0;
        }
    }

    [Description("How long the resize effect will last.")]
    [IntegerRange(-99, 99)]
    public int Resize_Level
    {
        get => _resized_level;
        set => _resized_level = value;
    }

    [Description("How long the freeze effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Freeze
    {
        get => _duration_freeze;
        set
        {
            _duration_freeze = value;
            if (_duration_freeze < 0)
                _duration_freeze = 0;
        }
    }

    [Description("How long the unnatural hunger effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Hunger
    {
        get => _duration_hunger;
        set
        {
            _duration_hunger = value;
            if (_duration_hunger < 0)
                _duration_hunger = 0;
        }
    }

    [Description("How long the charm effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Charm
    {
        get => _duration_charm;
        set
        {
            _duration_charm = value;
            if (_duration_charm < 0)
                _duration_charm = 0;
        }
    }

    [Description("How long the arousal effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_Aroused
    {
        get => _duration_aroused;
        set
        {
            _duration_aroused = value;
            if (_duration_aroused < 0)
                _duration_aroused = 0;
        }
    }

    [Description("How long the prey curse effect will last.")]
    [IntegerRange(0, 99)]
    public int Duration_PreyCurse
    {
        get => _duration_preycurse;
        set
        {
            _duration_preycurse = value;
            if (_duration_preycurse < 0)
                _duration_preycurse = 0;
        }
    }

    [VariableEditorIgnores]
    [OdinSerialize]
    public Person CharmedBy = null;


    [VariableEditorIgnores]
    public bool IsCharmedBy(Person Charmer)
    {
        if (Duration_Charm > 0 && CharmedBy == Charmer)
            return true;
        else
            return false;
    }

    public void Update()
    {
        if (Self.Dead)
            return;

        if (State.World.Settings.FlexibleStats)
        {
            Proficiency -= State.World.Settings.FlexibleStatDecaySpeed * 0.0001f;
            Willpower -= State.World.Settings.FlexibleStatDecaySpeed * 0.0001f;
        }

        if (CanUseMagic(State.World.ControlledPerson))
        {
            float boost = 0.01f;
            if (Self.VoreController.HasAnyPrey())
            {
                boost += 0.03f;
                if (Self.VoreController.IsDigestingAnyPrey())
                    boost += 0.03f;
            }

            if (Self.BeingEaten == false)
                Self.Magic.Mana += (
                    boost * State.World.Settings.ManaRegenMod * Self.Magic.ManaRegen
                );
            Self.Magic.Mana += (boost * Self.Boosts.ManaRegen);

            if (Self.VoreController.IsAbsorbing())
                Self.Magic.Mana += 0.2f * State.World.Settings.AbsorptionSpeed;
        }

        // RESET HEIGHT & WEIGHT
        if (Self.Magic.Duration_Resized == 1)
        {
            if (Self.Magic.Resize_Level < 0)
            {
                Self.Magic.Resize_Level += 1;
                SelfActionList.List[SelfActionType.ResetShrink].OnDo(Self);
            }
            else
            {
                Self.Magic.Resize_Level -= 1;
                SelfActionList.List[SelfActionType.ResetGrow].OnDo(Self);
            }
            if (Self.Magic.Resize_Level != 0)
            {
                Self.Magic.Duration_Resized +=
                    1 + (int)(12 * State.World.Settings.SpellDurationMod);
            }
        }

        if (Duration_Charm == 1 && CharmedBy != null)
            SelfActionList.List[SelfActionType.CharmEnded].OnDo(Self);

        // REDUCE DURATIONS

        Self.Magic.Duration_Passdoor -= 1;
        Self.Magic.Duration_Freeze -= 1;
        Self.Magic.Duration_Hunger -= 1;
        Self.Magic.Duration_Charm -= 1;
        Self.Magic.Duration_Aroused -= 1;
        Self.Magic.Duration_PreyCurse -= 1;

        if (State.World.Settings.PermaSizeChange == false)
        {
            Self.Magic.Duration_Resized -= 1;
        }
        Self.PartList.SizeDeltas["magic_size"] = Mathf.Pow(
            State.World.Settings.SizeChangeMod,
            Self.Magic.Resize_Level
        );
    }
}
