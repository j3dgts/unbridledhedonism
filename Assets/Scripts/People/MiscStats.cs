﻿using OdinSerializer;

namespace Assets.Scripts.People
{
    public class MiscStats
    {
        [OdinSerialize]
        [VariableEditorIgnores]
        public int TurnAdded;

        [OdinSerialize, ProperName("Current Weight Gained")]
        [Description("The % change in weight from vore over this character's lifetime")]
        [FloatRange(0, 1000)]
        public float CurrentWeightGain;

        [OdinSerialize, ProperName("Current Breast Gained")]
        [Description("The % change in breast size from vore over this character's lifetime")]
        [FloatRange(0, 1000)]
        public float CurrentBreastGain;

        [OdinSerialize, ProperName("Current Height Gained")]
        [Description("The % change in height from vore over this character's lifetime")]
        [FloatRange(0, 1000)]
        public float CurrentHeightGain;

        [OdinSerialize, ProperName("Current Dick Gained")]
        [Description("The % change in dick size from vore over this character's lifetime")]
        [FloatRange(0, 1000)]
        public float CurrentDickGain;

        [OdinSerialize, ProperName("Total Weight Gained")]
        [Description("The total amount of weight gained over the lifetime of this character")]
        [FloatRange(0, 1000)]
        public float TotalWeightGain;

        [OdinSerialize, ProperName("Total Weight Lost")]
        [Description("The total amount of weight lost over the lifetime of this character")]
        [FloatRange(0, 1000)]
        public float TotalWeightLoss;

        [OdinSerialize, ProperName("Times Swallowed Other")]
        [Description("The number of times this character has started swallowing someone else")]
        [IntegerRange(0, 1000)]
        public int TimesSwallowedOtherStart;

        [OdinSerialize, ProperName("Times Unbirthed Other")]
        [Description("The number of times this character has started unbirthing someone else")]
        [IntegerRange(0, 100)]
        public int TimesUnbirthedOtherStart;

        [OdinSerialize, ProperName("Times Anal Vored Other")]
        [Description("The number of times this character has started anal voring someone else")]
        [IntegerRange(0, 100)]
        public int TimesAnalVoredOtherStart;

        [OdinSerialize, ProperName("Times Cock Vored Other")]
        [Description("The number of times this character has started cock voring someone else")]
        [IntegerRange(0, 100)]
        public int TimesCockVoredOtherStart;

        [OdinSerialize, ProperName("Times Been Swallowed")]
        [Description("The number of times this character has started being swallowed")]
        [IntegerRange(0, 100)]
        public int TimesBeenSwallowedStart;

        [OdinSerialize, ProperName("Times Been Unbirthed")]
        [Description("The number of times this character has started being unbirthed")]
        [IntegerRange(0, 100)]
        public int TimesBeenUnbirthedStart;

        [OdinSerialize, ProperName("Times Been Anal Vored")]
        [Description("The number of times this character has started being anal vored")]
        [IntegerRange(0, 100)]
        public int TimesBeenAnalVoredStart;

        [OdinSerialize, ProperName("Times Been Cock Vored")]
        [Description("The number of times this character has started being cock vored")]
        [IntegerRange(0, 100)]
        public int TimesBeenCockVoredStart;

        [OdinSerialize, ProperName("Times Swallowed Other")]
        [Description(
            "The number of times this character has swallowed someone else.  Only counts if the consuming phase wasn't interrupted."
        )]
        [IntegerRange(0, 1000)]
        public int TimesSwallowedOther;

        [OdinSerialize, ProperName("Times Unbirthed Other")]
        [Description("The number of times this character has unbirthed someone else")]
        [IntegerRange(0, 100)]
        public int TimesUnbirthedOther;

        [OdinSerialize, ProperName("Times Anal Vored Other")]
        [Description("The number of times this character has anal vored someone else")]
        [IntegerRange(0, 100)]
        public int TimesAnalVoredOther;

        [OdinSerialize, ProperName("Times Cock Vored Other")]
        [Description("The number of times this character has cock vored someone else")]
        [IntegerRange(0, 100)]
        public int TimesCockVoredOther;

        [OdinSerialize, ProperName("Times Been Swallowed")]
        [Description("The number of times this character has been swallowed")]
        [IntegerRange(0, 100)]
        public int TimesBeenSwallowed;

        [OdinSerialize, ProperName("Times Been Unbirthed")]
        [Description("The number of times this character has been unbirthed")]
        [IntegerRange(0, 100)]
        public int TimesBeenUnbirthed;

        [OdinSerialize, ProperName("Times Been Anal Vored")]
        [Description("The number of times this character has been anal vored")]
        [IntegerRange(0, 100)]
        public int TimesBeenAnalVored;

        [OdinSerialize, ProperName("Times Been Cock Vored")]
        [Description("The number of times this character has been cock vored")]
        [IntegerRange(0, 100)]
        public int TimesBeenCockVored;

        [OdinSerialize, ProperName("Times Digested Other")]
        [Description("The number of times this character has digested or melted someone else.")]
        [IntegerRange(0, 100)]
        public int TimesDigestedOther;

        [OdinSerialize, ProperName("Times Been Digested")]
        [Description(
            "The number of times this character has been digested or melted.  (only applies for reformation, as anything else gets rid of the character)"
        )]
        [IntegerRange(0, 100)]
        public int TimesBeenDigested;
    }
}
