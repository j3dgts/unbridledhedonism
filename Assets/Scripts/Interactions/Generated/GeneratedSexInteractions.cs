using static InteractionFunctions;
using System.Collections.Generic;
using UnityEngine.Rendering.VirtualTexturing;

namespace GeneratedSexInteractions
{
    class StripSelf : SexInteractionBase
    {
        public StripSelf()
        {
            Name = "StripSelf";
            Description = "Strip off some of your clothes.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.StripSelf;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => a.ClothingStatus != ClothingStatus.Nude;
            Effect = (a, t) => Strip(a);
        }
    }

    class StripThem : SexInteractionBase
    {
        public StripThem()
        {
            Name = "StripThem";
            Description = "Strip off some of your partner's clothes.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.StripThem;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => t.ClothingStatus != ClothingStatus.Nude;
            Effect = (a, t) => Strip(t);
        }
    }

    class RubHair : SexInteractionBase
    {
        public RubHair()
        {
            Name = "RubHair";
            Description = "Gently rub their hair with your hands.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.RubHair;
            SoundRange = -1;
            Dominance = 0.7f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.01f, 0);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.BehindGiving,
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.BehindReceiving
            };
        }
    }

    class CaressFace : SexInteractionBase
    {
        public CaressFace()
        {
            Name = "CaressFace";
            Description = "Caress their face with your hands.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.CaressFace;
            SoundRange = -1;
            Dominance = 0.8f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.01f, 0);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.BehindGiving,
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.Missionary,
                SexPosition.BehindReceiving
            };
        }
    }

    class Massage : SexInteractionBase
    {
        public Massage()
        {
            Name = "Massage";
            Description = "Massage your partners neck or back.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Massage;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.01f, 0);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.BehindGiving
            };
        }
    }

    class DeepKiss : SexInteractionBase
    {
        public DeepKiss()
        {
            Name = "DeepKiss";
            Description = "Give your partner a long kiss.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.DeepKiss;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.01f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.Missionary
            };
        }
    }

    class FondleBreasts : SexInteractionBase
    {
        public FondleBreasts()
        {
            Name = "FondleBreasts";
            Description = "Rub your partner's breasts.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.FondleBreasts;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => t.GenderType.HasBreasts;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.SixtyNine,
                SexPosition.Missionary
            };
        }
    }

    class FondleButt : SexInteractionBase
    {
        public FondleButt()
        {
            Name = "FondleButt";
            Description = "Rub your partner's ass.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.FondleButt;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.SixtyNine
            };
        }
    }

    class KissBelly : SexInteractionBase
    {
        public KissBelly()
        {
            Name = "KissBelly";
            Description = "Kiss or lick your partner's belly.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.KissBelly;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.BellySmother };
            SoundRange = -1;
            Dominance = 0.3f;
            AppearConditional = (a, t) =>
                t.VoreController.BellySize() > 0;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.SixtyNine,
                SexPosition.Missionary
            };
        }
    }

    class KissBalls : SexInteractionBase
    {
        public KissBalls()
        {
            Name = "KissBalls";
            Description = "Kiss or lick your partner's balls.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.KissBalls;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.BallsSmother };
            SoundRange = -1;
            Dominance = 0.3f;
            AppearConditional = (a, t) =>
                t.GenderType.HasDick
                && t.VoreController.BallsSize() > 0;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.LyingDown,
                SexPosition.SixtyNine
            };
        }
    }

    class BellySmother : SexInteractionBase
    {
        public BellySmother()
        {
            Name = "BellySmother";
            Description = "Press your belly against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.BellySmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissBelly };
            SoundRange = -1;
            Dominance = 0.9f;
            AppearConditional = (a, t) =>
                a.VoreController.BellySize() > 0;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.SixtyNine,
                SexPosition.Missionary
            };
        }
    }

    class BallsSmother : SexInteractionBase
    {
        public BallsSmother()
        {
            Name = "BallsSmother";
            Description = "Press your balls against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.BallsSmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissBalls };
            SoundRange = -1;
            Dominance = 1.0f;
            AppearConditional = (a, t) =>
                a.GenderType.HasDick
                && a.VoreController.BallsSize() > 0;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.SixtyNine
            };
        }
    }

    class BreastsSmother : SexInteractionBase
    {
        public BreastsSmother()
        {
            Name = "BreastsSmother";
            Description = "Press your breasts against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.BreastsSmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissBreasts };
            SoundRange = -1;
            Dominance = 0.8f;
            AppearConditional = (a, t) =>
                a.GenderType.HasBreasts
                && a.PartList.BreastSize > 1.5f;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.Standing,
                SexPosition.LyingDown,
                SexPosition.SixtyNine,
                SexPosition.Missionary
            };
        }
    }

    class ButtSmother : SexInteractionBase
    {
        public ButtSmother()
        {
            Name = "ButtSmother";
            Description = "Press your ass against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.ButtSmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.Rimjob };
            SoundRange = -1;
            Dominance = 1.0f;
            AppearConditional = (a, t) =>
                true;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.007f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.SixtyNine
            };
        }
    }

    class KissArmpit : SexInteractionBase
    {
        public KissArmpit()
        {
            Name = "KissArmpit";
            Description = "Kiss or lick your partner's armpit.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.KissArmpit;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.ArmpitSmother };
            SoundRange = -1;
            Dominance = 0.0f;
            AppearConditional = (a, t) =>
                (a.HasTrait(Quirks.SexGuru) || a.HasTrait(Quirks.Olfactophilic) || (t.HasTrait(Quirks.Olfactophilic) && State.World.Settings.PartnerKinkySex));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.008f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class ArmpitSmother : SexInteractionBase
    {
        public ArmpitSmother()
        {
            Name = "ArmpitSmother";
            Description = "Press your armpit against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.ArmpitSmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissArmpit };
            SoundRange = -1;
            Dominance = 1.0f;
            AppearConditional = (a, t) =>
                (a.HasTrait(Quirks.SexGuru) || a.HasTrait(Quirks.Olfactophilic) || (t.HasTrait(Quirks.Olfactophilic) && State.World.Settings.PartnerKinkySex));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.008f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class KissFeet : SexInteractionBase
    {
        public KissFeet()
        {
            Name = "KissFeet";
            Description = "Kiss or lick your partner's feet.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.KissFeet;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.FootSmother };
            SoundRange = -1;
            Dominance = 0.1f;
            AppearConditional = (a, t) =>
                t.ClothingStatus != ClothingStatus.Normal
                && (a.HasTrait(Quirks.SexGuru) || a.HasTrait(Quirks.FootFetish) || (t.HasTrait(Quirks.FootFetish) && State.World.Settings.PartnerKinkySex));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.005f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.LyingDown,
                SexPosition.Missionary
            };
        }
    }

    class FootSmother : SexInteractionBase
    {
        public FootSmother()
        {
            Name = "FootSmother";
            Description = "Press your feet against your partner's face.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.FootSmother;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissFeet };
            SoundRange = -1;
            Dominance = 0.9f;
            AppearConditional = (a, t) =>
                a.ClothingStatus != ClothingStatus.Normal
                && (a.HasTrait(Quirks.SexGuru) || a.HasTrait(Quirks.FootFetish) || (t.HasTrait(Quirks.FootFetish) && State.World.Settings.PartnerKinkySex));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.005f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Missionary
            };
        }
    }

    class FootJob : SexInteractionBase
    {
        public FootJob()
        {
            Name = "FootJob";
            Description = "Press your feet against your partner's genitals.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.FootJob;
            SoundRange = -1;
            Dominance = 0.8f;
            AppearConditional = (a, t) =>
                a.ClothingStatus != ClothingStatus.Normal
                && t.ClothingStatus == ClothingStatus.Nude
                && (a.HasTrait(Quirks.SexGuru) || a.HasTrait(Quirks.FootFetish) || (t.HasTrait(Quirks.FootFetish) && State.World.Settings.PartnerKinkySex));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.005f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown
            };
        }
    }

    class Breastfeed : SexInteractionBase
    {
        public Breastfeed()
        {
            Name = "Breastfeed";
            Description = "Have your partner drink your breast milk.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Breastfeed;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.KissBreasts };
            SoundRange = -1;
            Dominance = 0.7f;
            AppearConditional = (a, t) =>
                a.GenderType.HasBreasts
                && a.ClothingStatus == ClothingStatus.Nude
                && (
                    a.HasTrait(Quirks.SexGuru)
                    || a.HasTrait(Quirks.Motherly)
                    || (a.VoreController.HasWombPrey() && State.World.Settings.UnbirthLactation)
                );
            Effect = (a, t) => Breastfeed(a, t, 0.02f, 0.02f, 0.05f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingReceiving,
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class KissBreasts : SexInteractionBase
    {
        public KissBreasts()
        {
            Name = "KissBreasts";
            Description = "Kiss or suck on your partner's breasts.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.KissBreasts;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.Breastfeed, SexInteractionType.BreastsSmother };
            SoundRange = -1;
            Dominance = 0.3f;
            AppearConditional = (a, t) =>
                t.GenderType.HasBreasts && t.ClothingStatus == ClothingStatus.Nude;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.025f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class Finger : SexInteractionBase
    {
        public Finger()
        {
            Name = "Finger";
            Description = "Stimulate their pussy with your hand.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Finger;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => t.GenderType.HasVagina;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.03f, 0.004f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.KneelingGiving,
                SexPosition.SixtyNine
            };
        }
    }

    class Stroke : SexInteractionBase
    {
        public Stroke()
        {
            Name = "Stroke";
            Description = "Stimulate their dick with your hand.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Stroke;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => t.GenderType.HasDick;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.03f, 0.004f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing,
                SexPosition.KneelingGiving,
                SexPosition.SixtyNine,
                SexPosition.BehindReceiving
            };
        }
    }

    class BlowJob : SexInteractionBase
    {
        public BlowJob()
        {
            Name = "BlowJob";
            Description = "Suck on their dick.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.BlowJob;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.RubHair, SexInteractionType.FaceFuck };
            SoundRange = -1;
            Dominance = 0.2f;
            AppearConditional = (a, t) =>
                t.GenderType.HasDick && t.ClothingStatus == ClothingStatus.Nude;
            Effect = (a, t) => SexEventSplit(a, t, 0.01f, 0.03f, 0.04f, 0.004f, 0.004f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.SixtyNine
            };
        }
    }

    class Cunnilingus : SexInteractionBase
    {
        public Cunnilingus()
        {
            Name = "Cunnilingus";
            Description = "Explore their pussy with your tongue.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Cunnilingus;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.RubHair, SexInteractionType.FaceFuck };
            SoundRange = -1;
            Dominance = 0.2f;
            AppearConditional = (a, t) =>
                t.GenderType.HasVagina && t.ClothingStatus == ClothingStatus.Nude;
            Effect = (a, t) => SexEventSplit(a, t, 0.01f, 0.03f, 0.04f, 0.004f, 0.004f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.KneelingGiving,
                SexPosition.SixtyNine
            };
        }
    }

    class SelfMasturbate : SexInteractionBase
    {
        public SelfMasturbate()
        {
            Name = "SelfMasturbate";
            Description = "Masturbate yourself during sex.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.SelfMasturbate;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => SexEventSplit(a, t, 0.01f, 0.03f, 0.01f, 0.004f, 0f);
        }
    }

    class Intercourse : SexInteractionBase
    {
        public Intercourse()
        {
            Name = "Intercourse";
            Description = "Use your sexual organs with theirs.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Intercourse;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.AnalGiving, SexInteractionType.AnalReceiving, SexInteractionType.VaginalGiving, SexInteractionType.VaginalReceiving, SexInteractionType.Frotting, SexInteractionType.Scissoring };
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && ((a.GenderType.HasVagina == false && a.GenderType.HasDick == false) || (t.GenderType.HasVagina == false && t.GenderType.HasDick == false));
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class Scissoring : SexInteractionBase
    {
        public Scissoring()
        {
            Name = "Scissoring";
            Description = "Rub your pussy against theirs.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Scissoring;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.Scissoring };
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && a.GenderType.HasVagina && t.GenderType.HasVagina;
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Missionary,
                SexPosition.Standing
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class VaginalGiving : SexInteractionBase
    {
        public VaginalGiving()
        {
            Name = "VaginalGiving";
            Description = "Fuck their pussy with your dick.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.VaginalGiving;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.VaginalReceiving };
            Dominance = 0.75f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && a.GenderType.HasDick && t.GenderType.HasVagina;
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Missionary,
                SexPosition.Standing,
                SexPosition.BehindGiving
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class VaginalReceiving : SexInteractionBase
    {
        public VaginalReceiving()
        {
            Name = "VaginalReceiving";
            Description = "Fuck their dick with your pussy.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.VaginalReceiving;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.VaginalGiving };
            Dominance = 0.25f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && a.GenderType.HasVagina && t.GenderType.HasDick;
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Missionary,
                SexPosition.Standing,
                SexPosition.BehindReceiving
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class AnalGiving : SexInteractionBase
    {
        public AnalGiving()
        {
            Name = "AnalGiving";
            Description = "Fuck their ass with your dick.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.AnalGiving;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.AnalReceiving };
            Dominance = 0.9f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && a.GenderType.HasDick;
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Missionary,
                SexPosition.Standing,
                SexPosition.BehindGiving,
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class AnalReceiving : SexInteractionBase
    {
        public AnalReceiving()
        {
            Name = "AnalReceiving";
            Description = "Fuck their dick with your ass.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.AnalReceiving;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.AnalGiving };
            Dominance = 0.1f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && t.GenderType.HasDick;
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Missionary,
                SexPosition.Standing,
                SexPosition.BehindReceiving
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class FaceFuck : SexInteractionBase
    {
        public FaceFuck()
        {
            Name = "FaceFuck";
            Description = "Fuck their mouth with your genitals.";
            Class = ClassType.Romantic;
            Type = SexInteractionType.FaceFuck;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.BlowJob, SexInteractionType.Cunnilingus };
            Dominance = 0.9f;
            AppearConditional = (a, t) =>
                a.ClothingStatus == ClothingStatus.Nude
                && (a.GenderType.HasDick || a.GenderType.HasVagina);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.KneelingReceiving,
                SexPosition.SixtyNine
            };
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
        }
    }

    class Frotting : SexInteractionBase
    {
        public Frotting()
        {
            Name = "Frotting";
            Description = "Engage in dick to dick contact";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Frotting;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.Frotting };
            Dominance = 0.5f;
            SoundRange = -1;
            AppearConditional = (a, t) => a.GenderType.HasDick && t.GenderType.HasDick;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.05f, 0.008f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class Rimjob : SexInteractionBase
    {
        public Rimjob()
        {
            Name = "Rimjob";
            Description = "Lick their ass";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Rimjob;
            PartnerActions = new List<SexInteractionType> { SexInteractionType.ButtSmother };
            Dominance = 0.1f;
            SoundRange = -1;
            AppearConditional = (a, t) => State.World.Settings.EnableRimjob;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.BehindGiving
            };
        }
    }

    class Navelfuck : SexInteractionBase
    {
        public Navelfuck()
        {
            Name = "Navelfuck";
            Description = "Fuck their navel";
            Class = ClassType.Romantic;
            Type = SexInteractionType.Navelfuck;
            Dominance = 0.8f;
            SoundRange = -1;
            AppearConditional = (a, t) => State.World.Settings.EnableNavelfuck;
            Effect = (a, t) => SexEvent(a, t, 0.01f, 0.02f, 0.002f);
            AllowedPositions = new List<SexPosition>()
            {
                SexPosition.LyingDown,
                SexPosition.Standing
            };
        }
    }

    class SwitchPositionToStanding : SexInteractionBase
    {
        public SwitchPositionToStanding()
        {
            Name = "SwitchPositionToStanding";
            Description = "Change positions so that you and your partner are both standing up.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToStanding;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => a.ActiveSex.Position != SexPosition.Standing;
            Effect = (a, t) => SwitchPosition(a, t, SexPosition.Standing);
        }
    }

    class SwitchPositionToLyingDown : SexInteractionBase
    {
        public SwitchPositionToLyingDown()
        {
            Name = "SwitchPositionToLyingDown";
            Description = "Change positions so that you and your partner are both lying down.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToLyingDown;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => a.ActiveSex.Position != SexPosition.LyingDown;
            Effect = (a, t) => SwitchPosition(a, t, SexPosition.LyingDown);
        }
    }

    class SwitchPositionToKneelingGiving : SexInteractionBase
    {
        public SwitchPositionToKneelingGiving()
        {
            Name = "SwitchPositionToKneelingGiving";
            Description = "Change positions so that you are kneeling in front of your partner.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToKneelingGiving;
            SoundRange = -1;
            Dominance = 0.8f;
            AppearConditional = (a, t) =>
                a.ActiveSex.Position != SexPosition.KneelingGiving;
            Effect = (a, t) =>
                SwitchPosition(a, t, SexPosition.KneelingGiving, SexPosition.KneelingReceiving);
        }
    }

    class SwitchPositionToKneelingReceiving : SexInteractionBase
    {
        public SwitchPositionToKneelingReceiving()
        {
            Name = "SwitchPositionToKneelingReceiving";
            Description = "Change positions so that your partner is kneeling in front of you.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToKneelingReceiving;
            SoundRange = -1;
            Dominance = 0.2f;
            AppearConditional = (a, t) =>
                a.ActiveSex.Position != SexPosition.KneelingReceiving;
            Effect = (a, t) =>
                SwitchPosition(a, t, SexPosition.KneelingReceiving, SexPosition.KneelingGiving);
        }
    }

    class SwitchPositionToSixtyNine : SexInteractionBase
    {
        public SwitchPositionToSixtyNine()
        {
            Name = "SwitchPositionToSixtyNine";
            Description =
                "Change positions so that you and your partner both have your mouths at the other's genitals.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToSixtyNine;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => a.ActiveSex.Position != SexPosition.SixtyNine;
            Effect = (a, t) => SwitchPosition(a, t, SexPosition.SixtyNine);
        }
    }

    class SwitchPositionToMissionary : SexInteractionBase
    {
        public SwitchPositionToMissionary()
        {
            Name = "SwitchPositionToMissionary";
            Description =
                "Change positions so that you and your partner are on top of each other, facing each other.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToMissionary;
            SoundRange = -1;
            Dominance = 0.5f;
            AppearConditional = (a, t) => a.ActiveSex.Position != SexPosition.Missionary;
            Effect = (a, t) => SwitchPosition(a, t, SexPosition.Missionary);
        }
    }

    class SwitchPositionToBehindGiving : SexInteractionBase
    {
        public SwitchPositionToBehindGiving()
        {
            Name = "SwitchPositionToBehindGiving";
            Description =
                "Change positions so that you are directly behind your partner, both facing the same direction.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToBehindGiving;
            SoundRange = -1;
            Dominance = 0.8f;
            AppearConditional = (a, t) =>
                a.ActiveSex.Position != SexPosition.BehindGiving
                && a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && a.GenderType.HasDick;
            Effect = (a, t) =>
                SwitchPosition(a, t, SexPosition.BehindGiving, SexPosition.BehindReceiving);
        }
    }

    class SwitchPositionToBehindReceiving : SexInteractionBase
    {
        public SwitchPositionToBehindReceiving()
        {
            Name = "SwitchPositionToBehindReceiving";
            Description =
                "Change positions so that your partner is directly behind you, both facing the same direction.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.SwitchPositionToBehindReceiving;
            SoundRange = -1;
            Dominance = 0.2f;
            AppearConditional = (a, t) =>
                a.ActiveSex.Position != SexPosition.BehindReceiving
                && a.ClothingStatus == ClothingStatus.Nude
                && t.ClothingStatus == ClothingStatus.Nude
                && t.GenderType.HasDick;
            Effect = (a, t) =>
                SwitchPosition(a, t, SexPosition.BehindReceiving, SexPosition.BehindGiving);
        }
    }

    class EndSex : SexInteractionBase
    {
        public EndSex()
        {
            Name = "EndSex";
            Description = "Stop having sex.";
            Class = ClassType.Friendly;
            Type = SexInteractionType.EndSex;
            SoundRange = -1;
            AppearConditional = (a, t) => true;
            Effect = (a, t) => EndSex(a, t);
        }
    }

    class KissVore : SexInteractionBase
    {
        public KissVore()
        {
            Name = "KissVore";
            Description = "Act like you're going to kiss your partner, then try to eat them.";
            Class = ClassType.VoreConsuming;
            Type = SexInteractionType.KissVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.Any);
            Effect = (a, t) => InteractionList.List[InteractionType.KissVore].RunCheck(a, t);
        }
    }

    class SexAskToOralVore : SexInteractionBase
    {
        public SexAskToOralVore()
        {
            Name = "SexAskToOralVore";
            Description = "Ask your partner if you can eat them (non-fatal is implied).";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToOralVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanEndo);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToOralVore].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToOralVoreDigest : SexInteractionBase
    {
        public SexAskToOralVoreDigest()
        {
            Name = "SexAskToOralVoreDigest";
            Description = "Ask your partner if you can eat them and digest them.";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToOralVoreDigest;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Oral, DigestionAlias.CanVore);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToOralVoreDigest].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexUnbirth : SexInteractionBase
    {
        public SexUnbirth()
        {
            Name = "SexUnbirth";
            Description = "Attempt to forcibly unbirth your partner.";
            Class = ClassType.VoreConsuming;
            Type = SexInteractionType.SexUnbirth;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.Any);
            Effect = (a, t) => InteractionList.List[InteractionType.SexUnbirth].RunCheck(a, t);
        }
    }

    class SexAskToUnbirth : SexInteractionBase
    {
        public SexAskToUnbirth()
        {
            Name = "SexAskToUnbirth";
            Description = "Ask your partner if you can unbirth them (non-fatal is implied).";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToUnbirth;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanEndo);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToUnbirth].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToUnbirthDigest : SexInteractionBase
    {
        public SexAskToUnbirthDigest()
        {
            Name = "SexAskToUnbirthDigest";
            Description = "Ask your partner if you can unbirth them and melt them.";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToUnbirthDigest;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Unbirth, DigestionAlias.CanVore);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToUnbirthDigest].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexCockVore : SexInteractionBase
    {
        public SexCockVore()
        {
            Name = "SexCockVore";
            Description = "Attempt to forcibly cock vore your partner.";
            Class = ClassType.VoreConsuming;
            Type = SexInteractionType.SexCockVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.Any);
            Effect = (a, t) => InteractionList.List[InteractionType.SexCockVore].RunCheck(a, t);
        }
    }

    class SexAskToCockVore : SexInteractionBase
    {
        public SexAskToCockVore()
        {
            Name = "SexAskToCockVore";
            Description = "Ask your partner if you can cock vore them (non-fatal is implied).";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToCockVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanEndo);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToCockVore].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToCockVoreDigest : SexInteractionBase
    {
        public SexAskToCockVoreDigest()
        {
            Name = "SexAskToCockVoreDigest";
            Description = "Ask your partner if you can cock vore them and melt them.";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToCockVoreDigest;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Cock, DigestionAlias.CanVore);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToCockVoreDigest].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAnalVore : SexInteractionBase
    {
        public SexAnalVore()
        {
            Name = "SexAnalVore";
            Description = "Attempt to forcibly anal vore your partner.";
            Class = ClassType.VoreConsuming;
            Type = SexInteractionType.SexAnalVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.Any);
            Effect = (a, t) => InteractionList.List[InteractionType.SexAnalVore].RunCheck(a, t);
        }
    }

    class SexAskToAnalVore : SexInteractionBase
    {
        public SexAskToAnalVore()
        {
            Name = "SexAskToAnalVore";
            Description = "Ask your partner if you can anal vore them (non-fatal is implied).";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToAnalVore;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanEndo);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToAnalVore].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToAnalVoreDigest : SexInteractionBase
    {
        public SexAskToAnalVoreDigest()
        {
            Name = "SexAskToAnalVoreDigest";
            Description = "Ask your partner if you can anal vore them and digest them.";
            Class = ClassType.VoreAskThem;
            Type = SexInteractionType.SexAskToAnalVoreDigest;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                a.VoreController.CouldVoreTarget(t, VoreType.Anal, DigestionAlias.CanVore);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.SexAskToAnalVoreDigest].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeOralVored : SexInteractionBase
    {
        public SexAskToBeOralVored()
        {
            Name = "SexAskToBeOralVored";
            Description = "Ask your partner to eat you and digest you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeOralVored;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeOralVored].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeOralVored].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeOralVoredEndo : SexInteractionBase
    {
        public SexAskToBeOralVoredEndo()
        {
            Name = "SexAskToBeOralVoredEndo";
            Description = "Ask your partner to eat you and not digest you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeOralVoredEndo;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeOralVoredEndo].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeOralVoredEndo].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeUnbirthed : SexInteractionBase
    {
        public SexAskToBeUnbirthed()
        {
            Name = "SexAskToBeUnbirthed";
            Description = "Ask your partner to unbirth you and melt you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeUnbirthed;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeUnbirthed].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeUnbirthed].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeUnbirthedEndo : SexInteractionBase
    {
        public SexAskToBeUnbirthedEndo()
        {
            Name = "SexAskToBeUnbirthedEndo";
            Description = "Ask your partner to unbirth you and not melt you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeUnbirthedEndo;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeUnbirthedEndo].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeUnbirthedEndo].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeCockVored : SexInteractionBase
    {
        public SexAskToBeCockVored()
        {
            Name = "SexAskToBeCockVored";
            Description = "Ask your partner to cock vore you and melt you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeCockVored;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeCockVored].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeCockVored].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeCockVoredEndo : SexInteractionBase
    {
        public SexAskToBeCockVoredEndo()
        {
            Name = "SexAskToBeCockVoredEndo";
            Description = "Ask your partner to cock vore you and not melt you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeCockVoredEndo;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeCockVoredEndo].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeCockVoredEndo].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeAnalVored : SexInteractionBase
    {
        public SexAskToBeAnalVored()
        {
            Name = "SexAskToBeAnalVored";
            Description = "Ask your partner to anal vore you and digest you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeAnalVored;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeAnalVored].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeAnalVored].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }

    class SexAskToBeAnalVoredEndo : SexInteractionBase
    {
        public SexAskToBeAnalVoredEndo()
        {
            Name = "SexAskToBeAnalVoredEndo";
            Description = "Ask your partner to anal vore you and not digest you.";
            Class = ClassType.VoreAskToBe;
            Type = SexInteractionType.SexAskToBeAnalVoredEndo;
            SoundRange = -1;
            AppearConditional = (a, t) =>
                InteractionList.List[InteractionType.AskToBeAnalVoredEndo].AppearConditional(a, t);
            Effect = (a, t) =>
                InteractionList.List[InteractionType.AskToBeAnalVoredEndo].RunCheck(
                    a,
                    t,
                    interruptOnFail: false
                );
        }
    }
}
