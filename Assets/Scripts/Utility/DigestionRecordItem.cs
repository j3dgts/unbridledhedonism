﻿using OdinSerializer;
using System.Collections.Generic;
using System.Linq;

class DigestionRecordItem
{
    [OdinSerialize]
    public Person Pred;

    [OdinSerialize]
    internal string OrigPred;

    [OdinSerialize]
    internal string OrigLocation;

    [OdinSerialize]
    public Person Prey;

    [OdinSerialize]
    public VoreLocation Location;

    [OdinSerialize]
    public bool Willing;

    [OdinSerialize]
    public bool OrigWilling;

    [OdinSerialize]
    public bool Betrayed;

    [OdinSerialize]
    internal bool WasDating;

    [OdinSerialize]
    internal string Dating;

    [OdinSerialize]
    internal List<VoreTrackingRecord> VoreRecord;

    [OdinSerialize]
    internal int VoreRecordCount;

    [OdinSerialize]
    internal VoreTrackingRecord FirstVoreRecord;

    [OdinSerialize]
    internal VoreTrackingRecord LastVoreRecord;

    [OdinSerialize]
    internal string EatenIn;

    [OdinSerialize]
    internal string EatenState;

    [OdinSerialize]
    internal bool PredAskedToEat;

    [OdinSerialize]
    internal bool PredAskedToDigest;

    [OdinSerialize]
    internal bool PreyAskedToBeEaten;

    [OdinSerialize]
    internal bool PreyAskedToBeDigested;

    [OdinSerialize]
    internal bool EatenDuringSex;

    [OdinSerialize]
    internal bool SamePred;

    [OdinSerialize]
    internal Person PrevPred;

    [OdinSerialize]
    internal string PrevPredLocation;

    [OdinSerialize]
    internal bool PredDigested;

    [OdinSerialize]
    internal bool PredReleased;

    [OdinSerialize]
    internal bool PreyEscaped;

    [OdinSerialize]
    internal bool PreyEatPrey;

    public DigestionRecordItem(Person pred, Person prey, VoreLocation location, bool willing)
    {
        Pred = pred;
        Prey = prey;
        Location = location;
        Willing = willing;
        if (prey.Romance.IsDating)
        {
            WasDating = true;
            Dating = prey.Romance.Dating.FirstName;
        }
        else
        {
            WasDating = false;
            Dating = null;
        }

        VoreRecord = prey.VoreTracking.ToList();
        VoreRecordCount = VoreRecord.Count();

        if (VoreRecordCount == 0)
        {
            UnityEngine.Debug.Log(
                $"{Prey.FirstName} was digested but their VoreRecord count was 0! Something that shouldn't happen."
            );
        }
        else
        {
            FirstVoreRecord = VoreRecord.FirstOrDefault();
            LastVoreRecord = VoreRecord.LastOrDefault();

            OrigPred = FirstVoreRecord.Pred.FirstName;
            OrigWilling = FirstVoreRecord.Willing;
            OrigLocation = FirstVoreRecord.Location.ToString().ToLower();

            switch (FirstVoreRecord.EatenIn)
            {
                case "OwnBedroom":
                    EatenIn = "in their own bedroom";
                    break;
                case "PredBedroom":
                    EatenIn = "in their pred's bedroom";
                    break;
                case "VacentBedroom":
                    EatenIn = "in a vacent bedroom";
                    break;
                case "Shower":
                    EatenIn = "in the shower";
                    break;
                case "Bathroom":
                    EatenIn = "in the bathroom";
                    break;
                case "Cafeteria":
                    EatenIn = "in the cafeteria";
                    break;
                case "Gym":
                    EatenIn = "in the gym";
                    break;
                case "Library":
                    EatenIn = "in the library";
                    break;
                case "NurseOffice":
                    EatenIn = "in the nurses office";
                    break;
                default:
                    EatenIn = "in a public place";
                    break;
            }
            switch (FirstVoreRecord.EatenState.ToString())
            {
                case "Nude":
                    EatenState = "naked";
                    break;
                case "Underwear":
                    EatenState = "in their underwear";
                    break;
                default:
                    EatenState = "fully clothed";
                    break;
            }

            if (VoreRecordCount == 1)
            {
                SamePred = true;
            }
            else
            {
                SamePred = false;
                PrevPred = LastVoreRecord.PrevPred;
                PrevPredLocation = VoreRecord
                    .ElementAt(VoreRecordCount - 2)
                    .Location.ToString()
                    .ToLower();

                if (LastVoreRecord.PrevPred != null)
                {
                    if (LastVoreRecord.PredDigested)
                    {
                        PredDigested = true;
                    }
                    else if (LastVoreRecord.PredReleased)
                    {
                        PredReleased = true;
                    }
                    else if (LastVoreRecord.PreyEscaped)
                    {
                        PreyEscaped = true;
                    }
                    else if (LastVoreRecord.PreyEatPrey)
                    {
                        PreyEatPrey = true;
                    }
                }
            }

            PredAskedToEat = LastVoreRecord.PredAskedToEat;
            PredAskedToDigest = LastVoreRecord.PredAskedToDigest;
            PreyAskedToBeEaten = LastVoreRecord.PreyAskedToBeEaten;
            PreyAskedToBeDigested = LastVoreRecord.PreyAskedToBeDigested;
            EatenDuringSex = LastVoreRecord.EatenDuringSex;
            Betrayed = LastVoreRecord.Betrayed;
        }
    }
}
