// All options have automatic defaults. You only need to set those options that you actually want to control.

// Min, Max Values
// Charisma, Strength, Voracity, SexDrive, PredWillingness, PreyWillingness, Promiscuity, Voraphilia,
// PredLoyalty, PreyDigestionInterest, Extroversion, OralVoreInterest, UnbirthInterest, CockVoreInterest, AnalVoreInterest
Charisma, 0.3, 0.7

// Weighted Options
// Cheats, Never, Rarely, Frequently
// CheatingAcceptance, None, Minor, Everything
// PreferredClothing, Normal, Underwear, Nude
// VorePreference, Either, Digestion, Endosoma
VorePreference, Masculine, 10, 0, 0
VorePreference, Feminine, 10, 0, 0

// Percent Options
// EndoDominator
EndoDominator, 0.10
