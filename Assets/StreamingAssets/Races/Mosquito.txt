Parent Race: Demi-Human
Selectable: True
Named Character: False

Hair Color, black, white, grey, red, orange, purple

Eye Color, black, white, red, orange, purple, yellow

Custom, Antenna Color, white, black, grey, red, orange

Custom, Chitin Color, white, black, grey, red

Height, Feminine, 46, 50
Height, Masculine, 42, 46

Weight, .85, .95

Breast Size, Feminine, 0, 3

Dick Size, Masculine, 0, 4
Dick Size, Feminine, 0, 4
//Ball sizes are tied to the dick size, defaults to 85-115%
Ball Size, Masculine, 1.00, 1.25
Ball Size, Feminine, 1.00, 1.25